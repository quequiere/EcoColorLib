﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcoColorLib
{
    public class ChatFormat
    {
        private ChatFormat(string value) { Value = value; }

        public string Value { get; set; }

        public static ChatFormat Green { get { return new ChatFormat("<#00cc00>"); } }
        public static ChatFormat Red { get { return new ChatFormat("<#FF0000>"); } }
        public static ChatFormat Blue { get { return new ChatFormat("<#0066ff>"); } }
        public static ChatFormat Yellow { get { return new ChatFormat("<#ffff00>"); } }

        public static ChatFormat Gray { get { return new ChatFormat("<#A0A0A0>"); } }
        public static ChatFormat ClearColor { get { return new ChatFormat("</color>"); } }



        public static ChatFormat Bold { get { return new ChatFormat("<b>"); } }
        public static ChatFormat ClearBold { get { return new ChatFormat("</b>"); } }

        public static ChatFormat UnderLine { get { return new ChatFormat("<u>"); } }
        public static ChatFormat ClearUnderLine { get { return new ChatFormat("</u>"); } }


        public static ChatFormat Italic { get { return new ChatFormat("<i>"); } }
        public static ChatFormat ClearItalic { get { return new ChatFormat("</i>"); } }

        public static ChatFormat Clear { get { return new ChatFormat("</u></i></b></color>"); } }
    }
}
